FROM java:8-jdk

MAINTAINER Ao Zeliu <aozeliu@otcaix.iscas.ac.cn>

ADD sandbox.tar.gz /opt/
ADD tomcat.tar.gz /opt/

COPY sandbox-collector/target/sandbox-collector-1.0-jar-with-dependencies.jar /root/.sandbox-module/

ENV JVM_AGENT="-javaagent:/opt/sandbox/lib/sandbox-agent.jar=server.ip=0.0.0.0;server.port=12345"
ENV JAVA_OPTS="-javaagent:/opt/sandbox/lib/sandbox-agent.jar=server.ip=0.0.0.0\;server.port=12345"

RUN rm /usr/bin/java
COPY java /usr/bin/java

ENV SANDBOX_HOME=/opt/sandbox
ENV TOMCAT_HOME=/opt/tomcat
ENV PATH=$PATH:$SANDBOX_HOME/bin:$TOMCAT_HOME/bin


