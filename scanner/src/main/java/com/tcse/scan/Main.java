package com.tcse.scan;

import com.tcse.common.TestCaseInfo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author aozeliu
 */
public class Main {

    public static void write(String filepath, List<TestCaseInfo> tests){
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(filepath))){
            for(TestCaseInfo test : tests){
                writer.println(test.getName());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println("Start scan........");

        String testDir = args[0];

        JunitTestDiscovery discovery = new JunitTestDiscovery();
        List<TestCaseInfo> tests = discovery.scan(testDir);

        write(args[1], tests);

        System.out.println("Finished scan........");
    }
}
