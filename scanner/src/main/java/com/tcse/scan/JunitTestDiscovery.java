package com.tcse.scan;

import com.tcse.common.TestCaseInfo;
import org.junit.platform.engine.TestSource;
import org.junit.platform.engine.support.descriptor.ClassSource;
import org.junit.platform.engine.support.descriptor.MethodSource;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherConfig;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClasspathRoots;

/**
 * @author aozeliu
 */
public class JunitTestDiscovery {

    private Launcher launcher;

    public JunitTestDiscovery(){
        initLauncher();
    }

    private void initLauncher(){
        LauncherConfig config = LauncherConfig.builder()
                .build();
        launcher = LauncherFactory.create(config);
    }


    private LauncherDiscoveryRequest getRequest(String testDir, String filterPattern){
        Set<Path> cps = new HashSet<>();
        cps.add(Paths.get(testDir));

        return LauncherDiscoveryRequestBuilder.request()
                .selectors(selectClasspathRoots(cps))
                .configurationParameter("junit.jupiter.extensions.autodetection.enabled", "true")
                .filters(includeClassNamePatterns(filterPattern == null ? ".*" : filterPattern))
                .build();
    }

    /*
     * junit在查找测试类时会尝试加载类
     * 所以testDir必须在classpath路径中
     * 否则会找不到测试
     */
    public List<TestCaseInfo> scan(String testDir){
        LauncherDiscoveryRequest request = getRequest(testDir, null);
        TestPlan plan = launcher.discover(request);

        List<TestCaseInfo> list = new ArrayList<>();

        Queue<TestIdentifier> q = new LinkedList<>(plan.getRoots());
        while (!q.isEmpty()){
            TestIdentifier test = q.poll();
            if(getClassSource(test, list)){
                for(TestIdentifier id : plan.getChildren(test)){
                    q.offer(id);
                }
            }
        }
        return list;
    }

    private TestSource getSource(Optional<TestSource> source){
        if(source.isPresent()){
            return source.get();
        }else {
            return null;
        }
    }

    /**
     * 以测试方法为一个测试用例
     */
    private boolean getMethodSource(TestIdentifier test, List<TestCaseInfo> list){
        TestSource source = getSource(test.getSource());
        if(source != null && source instanceof MethodSource){
            MethodSource methodSource = (MethodSource) source;
            TestCaseInfo info = new TestCaseInfo(test.getUniqueId());
            list.add(info);
            return false;
        }else{
            return true;
        }
    }

    /**
     * 以一个测试类为一个测试用例
     */
    private boolean getClassSource(TestIdentifier test, List<TestCaseInfo> list){
        TestSource source = getSource(test.getSource());
        if(source != null && source instanceof ClassSource){
            ClassSource classSource = (ClassSource) source;
            TestCaseInfo info = new TestCaseInfo(classSource.getClassName());
            list.add(info);
            return false;
        }else{
            return true;
        }
    }
}
