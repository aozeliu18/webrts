#!/usr/bin/env bash

source script/paper/webrts-env.sh

tomcatPid=`ps -ef | grep ${TOMCAT_BASE_DIR} | grep -v grep | awk '{print $2}'`

# 卸载sandbox
#${SANDBOX_BASE_DIR}/bin/sandbox.sh -p ${tomcatPid} -S
# 停止tomcat
#${TOMCAT_BASE_DIR}/bin/shutdown.sh

kill -9 ${tomcatPid}
