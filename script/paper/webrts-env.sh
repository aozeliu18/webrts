#!/usr/bin/env bash

# tomcat根目录
export TOMCAT_BASE_DIR=${TOMCAT_HOME-"/home/name/webrts/tomcat"}
# sandbox根目录
export SANDBOX_BASE_DIR=${SANDBOX_HOME-"/home/name/webrts/sandbox"}