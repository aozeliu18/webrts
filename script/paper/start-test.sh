#!/usr/bin/env bash

# 运行测试，本脚本用于收集时间信息
echo "start testing...."
time  script/junit/run.sh tests-target
echo "finished testing...."