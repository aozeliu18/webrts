#!/usr/bin/env bash

# 批量修改selenium IDE导出的Junit测试
# 使这些测试直接使用js执行命令

dir=$1

for f in `find ${dir}  -maxdepth 1 -type f`
do
    sed -i "s#\(.*\)\(driver.findElement(.*)\)\.click()#\1Utils.click(\2)#" ${f}
    sed -i "s#\(.*\)\(driver.findElement(.*)\)\.sendKeys(\(.*\))#\1Utils.sendKeys(\2, \3)#" ${f}
done