#!/usr/bin/env bash

# 插装类的模式
classPattern=$1

source script/paper/webrts-env.sh

cwd=`pwd`
echo "Start Directory : ${cwd}"

# 启动tomcat
cd ${TOMCAT_BASE_DIR} # 不得不切换到TOMCAT目录下
echo `pwd`
${TOMCAT_BASE_DIR}/bin/startup.sh
cd ${cwd}

# 获取tomcat进程的PID
# 一个tomcat目录下的tomcat只能启动一次
tomcatPid=`ps -ef | grep ${TOMCAT_BASE_DIR} | grep -v grep | awk '{print $2}'`
#tomcatPid=`jps | awk '$2=="Bootstrap" {print $1}'`
echo "Tomcat PID : ${tomcatPid}"

# 启动sandbox
echo "start sandbox...."
${SANDBOX_BASE_DIR}/bin/sandbox.sh -p ${tomcatPid} -P 12345
${SANDBOX_BASE_DIR}/bin/sandbox.sh -p ${tomcatPid} -d "collector/trace?class=${classPattern}"



