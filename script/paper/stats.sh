#!/usr/bin/env bash

# 本脚本用于统计验证项目的一些信息

# 文件数
find .  -type f -print  | wc -l

# 排除.git目录 
find . -path ./.git -prune -o -type f -print 

# 代码行数
find .  -type f -print | xargs cat | wc -l

find . -path ./.git -prune -o -type f -name "*.java" -print  | xargs cat | wc -l
java 17440
css 14390
js 1683 503
html 14262