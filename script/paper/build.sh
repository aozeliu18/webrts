#! /usr/bin/env bash

source script/paper/webrts-env.sh

# 编译被测应用
build_project(){
    sourceDir=$1

    cwd=`pwd`
    # 切换到应用源代码目录打包部署应用
    cd ${sourceDir}
    echo "Change Work Directory : `pwd`"
    mvn -DskipTests=true clean package &> /dev/null
    cd ${cwd}

    echo ${warPath}
}

# 复制编译后的应用代码
copy_project(){
    sourceDir=$1

    targetProjectDir=project-target
    warPath=`ls ${sourceDir}/target/*.war`  # 只能有一个war包
    rm -rf ${targetProjectDir}
    mkdir ${targetProjectDir}
    unzip -q ${warPath} -d ${targetProjectDir}
#    sed -i 's#^projectDir.*#projectDir='${targetProjectDir}'#g' conf/webrts.properties
    echo ${warPath}
}

# 部署项目
deploy_project(){
    warPath=$1

    rm -rf ${TOMCAT_BASE_DIR}/webapps/*
    cp ${warPath} ${TOMCAT_BASE_DIR}/webapps/
}

build_test(){
    testDir=$1

    cwd=`pwd`
    # 切换到测试源代码目录编译测试
    cd ${testDir}
    echo "Change Work Directory : `pwd`"
    mvn -DskipTests=true clean package &> /dev/null
    # 切回启动目录
    cd ${cwd}
}

# 复制编译后的测试代码
copy_test(){
    testDir=$1

    targetTestDir=tests-target
    rm -rf ${targetTestDir}
    mkdir ${targetTestDir}
    cp -r ${testDir}/target/classes ${targetTestDir}
    cp -r ${testDir}/target/test-classes ${targetTestDir}
    cp -r ${testDir}/target/libs ${targetTestDir}
}

project(){
    sourceDir=$1

    build_project ${sourceDir}
    warPath=$(copy_project ${sourceDir})
    deploy_project ${warPath}
}

tests(){
    testDir=$1
    
    build_test ${testDir}
    copy_test ${testDir}
}

while getopts 'p:t:' OPT; do
    case ${OPT} in
        p)
            project ${OPTARG}
            ;;
        t)
            tests ${OPTARG}
            ;;
        ?)
            echo "error options"
            ;;
    esac
done


