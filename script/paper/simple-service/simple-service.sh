#!/usr/bin/env bash

# 本脚本用于运行simple-service验证项目
#
# script/paper/simple-service.sh \
#        /home/name/azl/webtest/simple-service/  \
#        /home/name/azl/webtest/simple-service-test/ \
#        com.tcse.simple.*
# 
sourceDir=../webtest/simple-service/
testDir=../webtest/simple-service-test/
classPattern=com.tcse.simple.*

# 设置配置文件
cp conf/webrts-simple-service.properties conf/webrts.properties

# 构建被测项目及测试，并复制到当前目录下, 适用于传统的Java Web项目
# ${sourceDir} 指编译后的应用代码
# ${testDir} 指编译后的测试代码
# 以上两个代码，在原始目录编译后，会被复制当工作目录下的project-dir和test-dir下
script/paper/build.sh -p ${sourceDir}
script/paper/build.sh -t ${testDir}

# 启动tomcat, 并attach Agent
script/paper/start-tomcat.sh ${classPattern}

# 执行测试，并收集依赖信息
script/paper/start-test.sh

# 停止tomcat
script/paper/stop-tomcat.sh

