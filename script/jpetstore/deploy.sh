#!/usr/bin/env bash

cwd=`pwd`

function deploy_project() {
   # 如项目目录发生变化，则需要修相应的目录
  project_dir=./jpetstore/jpetstore-6
  # 切换到项目目录，编译代码
  cd ${project_dir}
  mvn clean package -DskipTests

  mkdir -p ${cwd}/project-target/
  # 将编译后的代码，复制到工作目录下
  cp -r target/jpetstore/* ${cwd}/project-target/
  
  # 将war部署到tomcat中
  mkdir -p ../../tomcat/webapps/
  cp target/jpetstore.war ../../tomcat/webapps/

  cd ${cwd} 
}

function deploy_test() {
   test_dir=./jpetstore/jpetstore-test
  cd ${test_dir}
  # 切换到测试目录，编译代码
  mvn clean package -DskipTests
  # 将编译后的测试代码及其依赖的jar包，复制到工作目录下
  mkdir -p ${cwd}/tests-target/test-classes ${cwd}/tests-target/libs
  cp -r target/test-classes/* ${cwd}/tests-target/test-classes/
  cp -r target/libs/* ${cwd}/tests-target/libs/
  cd ${cwd}
}

# -p 部署项目
# -t 部署测试
while getopts 'pt' OPT; do
    case ${OPT} in
        p)
            deploy_project
            ;;
        t)
            deploy_test
            ;;
        ?)
            echo "error options"
            ;;
    esac
done



