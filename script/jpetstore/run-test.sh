#!/usr/bin/env bash

# 1. Run the run-scanner.sh script to scan all existing tests
script/junit/run-scanner.sh tests-target tests

# 2. Run the run-select.sh script to select tests affected by changes
script/run-selector.sh

# 3. Run the run-junit.sh script to execute the selected tests
script/junit/run-junit.sh -t tests-target/ -i selected

# 4. Run the run-collector script to collect test dependency information
script/run-collector.sh

# 5. Run run-selector.sh again to detect whether the dependency has been successfully updated.
#    The test should not be selected when the update is successful
script/run-selector.sh
