#!/usr/bin/env bash


## 部署程序 deploy.sh -p
## 1. 编译jpetstore的代码
#mvn clean package -DskipTests
## 2. 将编译后的代码复制到project-target目录下
#cp -r ${project} project-target/
#
## 部署测试 deploy.sh -t
## 1. 编译jpetstore测试
#mvn clean package -DskipTests
## 2. 将测试及其依赖拷贝到tests-target目录下
#cp -r ${test} tests-target
## 3. 将jpetstore部署到Web容器中

script/jpetstore/deploy.sh -p -t
./tomcat/bin/startup.bat

## 运行
## 1. 运行run-scanner.sh脚本，扫描存在的测试
#script/junit/run-scanner.sh tests-target tests
#
## 2. 运行run-selector.sh脚本，选择测试
#script/run-selector.sh
#
## 3. 运行run-junit.sh脚本，执行被选择的测试
#script/junit/run-junit.sh -t tests-target/ -i selected
#
## 4. 运行run-collector脚本，收集依赖信息
#script/run-collector.sh
#
## 5.运行run-selector.sh，此时应没有测试被选择
#script/run-selector.sh
#

script/jpetstore/run-test.sh

## 6.切换到新版本代码，重复上述过程
#git checkout ${version}