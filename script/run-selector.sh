#!/usr/bin/env bash

#
#   运行选择模块脚本
#   配置完成配置文件后，直接运行本脚本即可
#

SELECTOR_JAR="selector/target/selector-1.0-jar-with-dependencies.jar"

# 指定配置文件位置
CONFIG_FILE=conf/webrts.properties

java -Dwebrts.config=${CONFIG_FILE} \
    -cp ${SELECTOR_JAR} com.tcse.select.Main