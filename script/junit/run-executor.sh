#!/usr/bin/env bash

SEP=";"

echo `pwd`

testBuildDir=$1

# 拼接路径
addClasspath() {
    basePath=$1
    path=""
    for jar in `ls ${basePath}`; do
        path="${path}${SEP}${basePath}/${jar}"
    done
    echo ${path:1}
}

JUNIT_JAR="script/junit/lib/junit-platform-console-standalone-1.5.2.jar"
EXECUTOR_JAR="executor/target/executor-1.0-jar-with-dependencies.jar"
classpath="${JUNIT_JAR}${SEP}${EXECUTOR_JAR}"
classpath="${classpath}${SEP}$(addClasspath ${testBuildDir}/libs)${SEP}${testBuildDir}/classes${SEP}${testBuildDir}/test-classes"
#echo ${classpath}

java \
    -cp ${classpath} com.tcse.execute.Main \
    tests
