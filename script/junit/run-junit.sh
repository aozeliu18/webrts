#!/usr/bin/env bash

SEP=";"

echo `pwd`

# 拼接路径
addClasspath() {
    basePath=$1
    path=""
    for jar in `ls ${basePath}`; do
        path="${path}${SEP}${basePath}/${jar}"
    done
    echo ${path:1}
}

JUNIT_JAR="script/junit/lib/junit-platform-console-standalone-1.5.2.jar"


#
# -d 目标测试编译后所在的目录，包括两个子目录test-classes：编译后的测试类，libs：测试依赖的jar包
#
# -j JVM参数，比如webdriver.chrome.driver
#
# example:
#   /usr/bin/time -f "%e" script/junit/run-junit.sh -t tests-target/ -i tests

while getopts 't:j:i:' OPT; do
    case ${OPT} in
        t)
            testBuildDir=${OPTARG};;
        j)
            JVM_ARGS=${OPTARG};;
        i)
            input=${OPTARG};;
     esac
done
# 移动命令行参数
# shift $(($OPTIND - 1))


classpath="${JUNIT_JAR}${SEP}$(addClasspath ${testBuildDir}/libs)${SEP}${testBuildDir}/test-classes"
#echo ${classpath}

classes=$(sed ':a ; N;s/\n/|/ ; t a ; ' ${input})
#echo ${classes}

java ${JVM_ARGS} \
    -cp "${classpath}" org.junit.platform.console.ConsoleLauncher \
    --disable-banner --disable-ansi-colors --scan-classpath \
    --include-classname=${classes}
