#!/usr/bin/env bash

#
# 当测试环境搭建好后（Web服务器已经部署，Agent已经attach）
# 执行本脚本
#       发现测试
#       选择测试
#       执行测试
#       收集依赖
#

# 测试代码所在目录，用于发现测试，执行测试
testsDir=$1
# 候选选择文件列表，发现模块会把存在的测试写入该文件
testsFile="tests"
# 被选择的测试列表，选择模块会把选择结果写入该文件
selectedFile="selected"


# 运行scanner寻找候选测试
script/junit/run-scanner.sh ${testsDir} ${testsFile}
echo "Total Test : $(cat ${testsFile} | wc -l)"

# 运行selector选择测试
script/run-selector.sh

# 运行测试
# /usr/bin/time -f "Test Runing Time Cost %e s" 
script/junit/run-junit.sh -t ${testsDir} -i ${selectedFile} -j -DHOST-PORT=192.168.209.126:8080

# 运行collector收集依赖信息
script/run-collector.sh
