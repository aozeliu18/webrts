#!/usr/bin/env bash

SEP=";"

addClasspath() {
    basePath=$1
    path=""
    for jar in `ls ${basePath}`; do
        path="${path}${SEP}${basePath}/${jar}"
    done
    echo ${path:1}
}


testBuildDir=$1
output=$2

JUNIT_JAR="script/junit/lib/junit-platform-console-standalone-1.5.2.jar"
SCANNER_JAR="scanner/target/scanner-1.0-jar-with-dependencies.jar"

classpath="${JUNIT_JAR}${SEP}${SCANNER_JAR}"
classpath="${classpath}${SEP}$(addClasspath ${testBuildDir}/libs)${SEP}${testBuildDir}/classes${SEP}${testBuildDir}/test-classes"

#echo ${classpath}

java -cp ${classpath} com.tcse.scan.Main "${testBuildDir}/test-classes" ${output}

