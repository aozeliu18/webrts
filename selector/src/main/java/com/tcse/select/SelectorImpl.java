package com.tcse.select;

import com.tcse.common.FileRecord;
import com.tcse.common.Hasher;
import com.tcse.common.Record;
import com.tcse.common.TestCaseInfo;
import com.tcse.store.Storer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * @author aozeliu
 */
public class SelectorImpl implements Selector{

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectorImpl.class);

    private String projectDir;

    private Storer storer;
    private Hasher hasher;

    public SelectorImpl(String projectDir, Storer storer, Hasher hasher) {
        this.projectDir = projectDir;
        this.storer = storer;
        this.hasher = hasher;
    }

    public List<TestCaseInfo> select(List<TestCaseInfo> tests){
        List<TestCaseInfo> selected = new ArrayList<>();

        for(TestCaseInfo test : tests){
            if(select(test)){
                LOGGER.debug("{} was selected......", test.getName());
                selected.add(test);
            }else{
                LOGGER.warn(test.getName() + " isn't selected");
            }
        }
        return selected;
    }


    private boolean select(TestCaseInfo test) {
        Set<Record> coverage = storer.get(test);

        if(coverage == null || coverage.isEmpty()){
            // 为空一般表示，这是一个新加的测试，所以需要被选择
            LOGGER.debug("{}'s dependencies are empty or null", test.getName());
            return true;
        }

        for(Record entry : coverage){
            if(isAffected((FileRecord) entry)){
                LOGGER.debug(test.getName() + " File Changed " + ((FileRecord) entry).getPath());
                return true;
            }
        }
        return false;
    }

    private boolean isAffected(FileRecord record) {
        File file = new File(projectDir, record.getPath());

        String oldChecksum = record.getChecksum();
        String newChecksum = hasher.hash(file);

        return !newChecksum.equals(oldChecksum);
    }
}
