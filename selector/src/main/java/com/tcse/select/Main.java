package com.tcse.select;

import com.tcse.common.Hasher;
import com.tcse.common.HasherImpl;
import com.tcse.common.RTSConfig;
import com.tcse.common.TestCaseInfo;
import com.tcse.store.HttpJsonStorer;
import com.tcse.store.Storer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author aozeliu
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static List<TestCaseInfo> read(String filepath){
        List<TestCaseInfo> tests = new ArrayList<>();
        try (Scanner scanner = new Scanner(new FileInputStream(filepath))){
            while (scanner.hasNext()){
                String testName = scanner.nextLine();
                tests.add(new TestCaseInfo(testName));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return tests;
    }

    public static void write(String filepath, List<TestCaseInfo> selected){
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(filepath))){
            for(TestCaseInfo test : selected){
                writer.println(test.getName());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        LOGGER.info("Start select............");
        long start = System.currentTimeMillis();

        if(args.length == 1){
            RTSConfig.loadConfig(args[0]);
        }else{
            RTSConfig.loadConfig(System.getProperty("webrts.config"));
        }

        String projectDir = RTSConfig.getConfig("projectDir");

        String depsPath = RTSConfig.getConfig("depsPath");
        Storer storer = new HttpJsonStorer(depsPath);

        Hasher hasher = new HasherImpl();

        Selector selector = new SelectorImpl(projectDir, storer, hasher);

        // 读取测试列表
        String testsFile = RTSConfig.getConfig("tests");
        List<TestCaseInfo> tests = read(testsFile);

        // 选择测试
        List<TestCaseInfo> selected = selector.select(tests);

        // 输出被选择的测试
        String selectedFile = RTSConfig.getConfig("selected");
        write(selectedFile, selected);

        long end = System.currentTimeMillis();
        LOGGER.info("Total Time Cost : " + (end - start) + "ms");
        LOGGER.info("Finished select............");
    }
}
