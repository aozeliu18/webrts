package com.tcse.select;

import com.tcse.common.TestCaseInfo;

import java.util.List;

/**
 * @author aozeliu
 *
 * Selector的作用只有一个，就是选择测试用例
 *      Selector接收一个候选测试列表，然后根据依赖变动信息，返回被影响的测试列表
 */
public interface Selector {

    /**
     * 选择测试
     * @param tests 候选测试集合
     * @return 被影响的测试集合
     */
    List<TestCaseInfo> select(List<TestCaseInfo> tests);

}
