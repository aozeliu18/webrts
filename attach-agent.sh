#! /bin/bash

# 批量删除镜像
# docker rmi $(docker images | grep "sqshq/piggymetrics-*" | grep -v "sqshq/piggymetrics-mongodb" | awk '{print $3}')

# 目标JVM的启动命令参数
TARGET_COMMAND_PATTERN="java -Xmx200m -jar"
# 找出目标JVM的进程ID，也可以使用jps程序
TARGET_JVM_PID=`ps -ef | grep "${TARGET_COMMAND_PATTERN}" | grep -v grep | awk '{print $2}'`

# 使用JVM-sandbox必须按照JDK，仅有JRE是不行的
SANDBOX_PORT=12345
CLASS_PATTERN=
sandbox.sh -p ${TARGET_JVM_PID} -P ${SANDBOX_PORT}
sandbox.sh -p ${TARGET_JVM_PID} -d "collector/trace?class=${CLASS_PATTERN}" 