package com.tcse.common;

import java.io.File;

/**
 * @author aozeliu
 */
public interface Hasher {
    String hash(File file);
}
