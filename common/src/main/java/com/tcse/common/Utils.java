package com.tcse.common;

/**
 * @author aozeliu
 */
public class Utils {

    public static String printStackTrace(String msg){
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();

        StringBuilder sb = new StringBuilder(msg);
        for(StackTraceElement element : stackTraceElements){
            sb.append("\n ")
                    .append(element.getClassName())
                    .append(".")
                    .append(element.getMethodName())
                    .append(".(")
                    .append(element.getFileName())
                    .append(".")
                    .append(element.getLineNumber())
                    .append(")");
        }
        return sb.toString();
    }
}
