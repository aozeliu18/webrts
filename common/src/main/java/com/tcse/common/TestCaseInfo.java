package com.tcse.common;

import java.util.Collection;
import java.util.Objects;

/**
 * @author aozeliu
 *
 * 描述了一个测试用例的信息：
 * 1. 测试用例本身的信息，如名称
 * 2. 测试用例依赖信息，这里依赖信息是文件级别的
 */
public class TestCaseInfo{

    private String name;
    private Collection<Record> records;

    public TestCaseInfo(String name) {
        this.name = name;
    }

    public Collection<Record> getRecords() {
        return records;
    }

    public void setRecords(Collection<Record> records) {
        this.records = records;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestCaseInfo info = (TestCaseInfo) o;
        return name.equals(info.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
