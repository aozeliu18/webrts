package com.tcse.common;

import java.util.Objects;

/**
 * @author aozeliu
 */
public class FileRecord extends Record{

    /**
     * 相对于war包的路径
     */
    private String path;

    private String checksum;

    public FileRecord(String path) {
        this(path, null);
    }

    public FileRecord(String path, String checksum) {
        this.path = path;
        this.checksum = checksum;

        setType();
    }

    public String getPath() {
        return path;
    }

    public String getChecksum() {
        return checksum;
    }

    @Override
    public void setType() {
        this.type = "file";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileRecord that = (FileRecord) o;
        return Objects.equals(path, that.path) &&
                Objects.equals(checksum, that.checksum);
    }   

    @Override
    public int hashCode() {
        return Objects.hash(path, checksum);
    }
}
