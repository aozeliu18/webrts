package com.tcse.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author aozeliu
 */
public class GsonUtils {

    public static List<HttpTrace> transform(String data){
        RuntimeTypeAdapterFactory adapterFactory =  RuntimeTypeAdapterFactory.of(Record.class);
        adapterFactory.registerSubtype(FileRecord.class, "file");
        adapterFactory.registerSubtype(MethodRecord.class, "method");

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(adapterFactory)
                .create();

        List<HttpTrace> list = gson.fromJson(data, new TypeToken<List<HttpTrace>>(){}.getType());
        setType(list);
        return list;
    }

    /**
     * gson多态反序化时，不设置类型字段的值
     * 故需要手动设置
     * @param list
     */
    private static void setType(List<HttpTrace> list) {
        if(list == null){
            return;
        }
        for(HttpTrace trace : list){
            Set<Record> records = trace.getRecords();
            for(Record record : records){
                record.setType();
            }
        }
    }

    public static String transform(Collection<HttpTrace> traces){
        Gson gson = new Gson();
        String data = gson.toJson(traces);
        return data;
    }
}
