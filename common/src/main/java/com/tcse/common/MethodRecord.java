package com.tcse.common;

import java.util.List;
import java.util.Objects;

/**
 * @author aozeliu
 */
public class MethodRecord extends Record{

    // 方法所在类
    private String methodClass;
    // 方法名
    private String methodName;
    // 方法返回值类型
    private String methodReturn;
    // 方法参数类型
    private List<String> methodParams;
    // 执行方法的实例对象类型, 如果为静态方法，此项为null
    private String instanceClass;


    public MethodRecord(String methodClass, 
                                String methodName, 
                                String methodReturn, 
                                List<String> methodParams,
            String instanceClass) {
        this.methodClass = methodClass;
        this.methodName = methodName;
        this.methodReturn = methodReturn;
        this.methodParams = methodParams;
        this.instanceClass = instanceClass;

        setType();
    }

    public String getMethodClass() {
        return methodClass;
    }

    public String getInstanceClass() {
        return instanceClass;
    }

    @Override
    public void setType() {
        this.type = "method";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MethodRecord that = (MethodRecord) o;
        return Objects.equals(methodClass, that.methodClass) &&
                Objects.equals(methodName, that.methodName) &&
                Objects.equals(methodReturn, that.methodReturn) &&
                Objects.equals(methodParams, that.methodParams) &&
                Objects.equals(instanceClass, that.instanceClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(methodClass, methodName, methodReturn, methodParams, instanceClass);
    }

    @Override
    public String toString() {
        return "RuntimeMethodRecord{" +
                "methodClass='" + methodClass + '\'' +
                ", methodName='" + methodName + '\'' +
                ", methodReturn='" + methodReturn + '\'' +
                ", methodParams=" + methodParams +
                ", instanceClass='" + instanceClass + '\'' +
                '}';
    }
}
