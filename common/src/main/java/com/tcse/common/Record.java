package com.tcse.common;

/**
 * @author aozeliu
 */
public abstract class Record {
    protected String type;

    public abstract void setType();
}
