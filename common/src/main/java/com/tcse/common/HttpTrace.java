package com.tcse.common;

import java.util.HashSet;
import java.util.Set;

/**
 * @author aozeliu
 */
public class HttpTrace {

    private String test;
    private HttpInfo httpInfo;

    private Set<Record> records = new HashSet<>();

    public HttpTrace(String test, HttpInfo httpInfo){
        this.test = test;
        this.httpInfo = httpInfo;
    }

    public boolean add(Record record){
        return records.add(record);
    }

    public Set<Record> getRecords() {
        return records;
    }

    public void setRecords(Set<Record> records) {
        this.records = records;
    }

    public String getTest() {
        return test;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((httpInfo == null) ? 0 : httpInfo.hashCode());
        result = prime * result + ((test == null) ? 0 : test.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpTrace other = (HttpTrace) obj;
        if (httpInfo == null) {
            if (other.httpInfo != null)
                return false;
        } else if (!httpInfo.equals(other.httpInfo))
            return false;
        if (test == null) {
            if (other.test != null)
                return false;
        } else if (!test.equals(other.test))
            return false;
        return true;
    }
}
