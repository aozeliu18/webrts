package com.tcse.common;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author aozeliu
 */
public class PathUtils {

    public static boolean isMatched(File file) {
        if (file.isDirectory()) {
            return false;
        }

        String path = null;
        try {
            path = file.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return path != null && !isJarFile(path) && !isClassFile(path);
    }

    public static final String[] JDK_CLASS = new String[] { "java/lang", "java.lang", "java/io", "java.io", "java/util",
            "java.util", "/javax/", "org/apache", "org.apache" };

    public static boolean isJDKClass(String name) {
        for (String s : JDK_CLASS) {
            if (name.contains(s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否记录文件
     * 
     * @param file
     * @return
     */
    public static boolean isInclude(File file) {
        if(file.isDirectory()){
            return false;
        }

        String path = null;
        try {
            path = file.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isInclude(path);
    }

    public static boolean isInclude(String path) {
        if (path == null || path.isEmpty()) {
            return false;
        }
        return !(isClassFile(path) || isJarFile(path) || isInJarFile(path) || isTmpFile(path));
    }

    private static boolean isInJarFile(String path) {
        return path.startsWith("jar");
    }

    public static boolean isClassFile(String path) {
        return path.endsWith(".class");
    }

    public static boolean isJarFile(String path) {
        return path.endsWith(".jar");
    }

    public static boolean isTmpFile(String path) {
        return path.startsWith("/tmp");
    }

    public static String getClasspath(Class<?> clazz) {
        String parent = clazz.getResource("").getPath();
        return parent + clazz.getSimpleName() + ".class";
    }

    // TODO 完善路径匹配
    public static String getClassPath(String path) {
        //return "WEB-INF/classes/" + path.replace(".", "/") + ".class";
        return getRelatedPath(path);
    }

    public static String getRelatedPath(String path) {
        String real;
        try {
            real = new File(path).toURI().getPath();
        } catch (Exception e) {
            e.printStackTrace();
            return path;
        }
        for(Map.Entry<String, String> entry : RTSConfig.getReplacePrefix().entrySet()){
            String prefix = entry.getKey();
            int index = real.indexOf(prefix);
            if(index != -1){
                return entry.getValue().concat(real.substring(index + prefix.length()));
            }
            
        }
        return real;
    }

    public static String getRelativePath(File file, String pattern){
        Pattern p = Pattern.compile(pattern);

        String path = "";
        try {
            path = file.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Matcher matcher = p.matcher(path);
        if(matcher.matches()){
            return matcher.group(1);
        }{
            return null;
        }

    }

    public static boolean exists(String path){
        File f = new File(path);
        return f.exists();
    }
}
