package com.tcse.common;

import java.io.*;
import java.net.URL;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

/**
 * @author aozeliu
 *
 * Hashing bytes
 */
public class HasherImpl implements Hasher{

    /**
     * Hash value in case of an error/exception
     */
    protected static final String ERROR_HASH = "-1";

    private Checksum checksum;

    public HasherImpl() {
        checksum = new Adler32();
    }


    public String hash(File file){
        if(file == null || !file.exists()){
            return ERROR_HASH;
        }
        byte[] bytes = loadBytes(file);
        String hash = hash(bytes);
        return hash;
    }


    public String hash(String str){
        if(str == null){
            return ERROR_HASH;
        }
        return hash(str.getBytes());
    }

    /**
     * Hashes byte array.
     */
    protected String hash(byte[] data) {
        if(data == null){
            return ERROR_HASH;
        }
        checksum.reset();
        checksum.update(data, 0, data.length);
        return Long.toString(checksum.getValue());
    }



    public static byte[] loadBytes(File file) {
        byte[] bytes = null;
        try (InputStream in = new FileInputStream(file)){
            bytes = loadBytes(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 对于字节码文件，移除DEBUG信息
        if(file.getName().endsWith(".class")){
            bytes = BytecodeCleaner.removeDebugInfo(bytes);
        }

        return bytes;
    }


    public static byte[] loadBytes(InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(1000);
        byte[] b = new byte[1000];
        int n;
        while ((n = in.read(b)) != -1) {
            out.write(b, 0, n);
        }
        out.close();
        return out.toByteArray();
    }

}