package com.tcse.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author aozeliu
 */
public class RTSConfig {

    private static Properties config;

    public static void loadConfig(String configPath){

        InputStream inputStream = null;

        if(configPath == null || !new File(configPath).exists()){
            configPath = "conf/webrts.properties";
        }

        config = new Properties();

        try{
            inputStream = new FileInputStream(configPath);
            config.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        parseReplacePrefix(config.getProperty("replacePrefix", ""));
    }

    /*
     * 环境变量配置 高于 文件配置
     */
    public static String getConfig(String key){
        String value = System.getProperty(key);
        if(value == null){
            if(config == null){
                System.err.println("Don't load configuration file");
                return null;
            }
            return config.getProperty(key);
        }else {
            return value;
        }
    }

    private static Map<String, String> replacePrefix = new HashMap<>();

    private static void parseReplacePrefix(String str){
        String[] items = str.split(",");
        if(items != null){
            for(String item : items){
                String[] tmp = item.split("=");
                replacePrefix.put(tmp[0], tmp[1]);
            }
        }
    }

    public static Map<String, String> getReplacePrefix(){
        return replacePrefix;
    }
}
