package com.tcse.pinpoint;

import com.navercorp.pinpoint.bootstrap.instrument.InstrumentClass;
import com.navercorp.pinpoint.bootstrap.instrument.InstrumentException;
import com.navercorp.pinpoint.bootstrap.instrument.InstrumentMethod;
import com.navercorp.pinpoint.bootstrap.instrument.Instrumentor;
import com.navercorp.pinpoint.bootstrap.instrument.matcher.Matchers;
import com.navercorp.pinpoint.bootstrap.instrument.transformer.*;
import com.navercorp.pinpoint.bootstrap.plugin.ProfilerPlugin;
import com.navercorp.pinpoint.bootstrap.plugin.ProfilerPluginSetupContext;

import java.security.ProtectionDomain;
import java.util.List;

public class WebRTSPlugin implements ProfilerPlugin, MatchableTransformTemplateAware {

    private static String BASE_PACKAGE = System.getProperty("webrts.agent.base.package", "com.tcse.simple");

    private ClassLoader classLoader;

    private MatchableTransformTemplate matchableTransformTemplate;


    @Override
    public void setup(ProfilerPluginSetupContext profilerPluginSetupContext) {
        classLoader = WebRTSPlugin.class.getClassLoader();
        addTransformers();
    }

    private void addTransformers(){
        matchableTransformTemplate.transform(Matchers.newPackageBasedMatcher(BASE_PACKAGE), new TransformCallback() {
            @Override
            public byte[] doInTransform(Instrumentor instrumentor, ClassLoader classLoader, String s, Class<?> aClass, ProtectionDomain protectionDomain, byte[] bytes) throws InstrumentException {

                if(s.contains("CGLIB")){
                    return null;
                }

                InstrumentClass instrumentClass = instrumentor.getInstrumentClass(WebRTSPlugin.this.classLoader, s, protectionDomain, bytes);
                List<InstrumentMethod> declaredMethods = instrumentClass.getDeclaredMethods();
                for (InstrumentMethod m : declaredMethods) {
                    m.addInterceptor("com.tcse.pinpoint.interceptor.WebRTSAppInterceptor");
                }
                return instrumentClass.toBytecode();
            }
        });

        matchableTransformTemplate.transform("java.io.File", new TransformCallback() {
            @Override
            public byte[] doInTransform(Instrumentor instrumentor, ClassLoader classLoader, String s, Class<?> aClass, ProtectionDomain protectionDomain, byte[] bytes) throws InstrumentException {
                InstrumentClass instrumentClass = instrumentor.getInstrumentClass(classLoader, s, protectionDomain, bytes);

                List<InstrumentMethod> declaredMethods = instrumentClass.getDeclaredMethods();
                for(InstrumentMethod m : declaredMethods){
                    if(m.getName().equals("File")){
                        m.addInterceptor("com.tcse.pinpoint.interceptor.WebRTSFileInterceptor");
                    }
                }
                return instrumentClass.toBytecode();
            }
        });
    }

    @Override
    public void setTransformTemplate(MatchableTransformTemplate matchableTransformTemplate) {
        this.matchableTransformTemplate = matchableTransformTemplate;
    }
}
