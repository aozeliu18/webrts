package com.tcse.pinpoint.interceptor;

import com.navercorp.pinpoint.bootstrap.context.*;
import com.navercorp.pinpoint.bootstrap.interceptor.AroundInterceptor;
import com.tcse.common.entity.ClassRecord;
import com.tcse.common.JsonUtils;
import com.tcse.common.entity.Record;
import com.tcse.pinpoint.RecordManager;
import com.tcse.pinpoint.ThreadContextStack;
import com.tcse.pinpoint.WebRTSTypeProvider;

import java.util.HashSet;
import java.util.Set;


public class WebRTSAppInterceptor implements AroundInterceptor {

    private static boolean ENABLE_CONSTRUCTOR = Boolean.valueOf(System.getProperty("webrts.constructor.enable", "true"));

    private final TraceContext traceContext;
    private final MethodDescriptor descriptor;
    private boolean isConstructor;

    public WebRTSAppInterceptor(TraceContext traceContext, MethodDescriptor descriptor) {
        this.traceContext = traceContext;
        this.descriptor = descriptor;

        isConstructor = descriptor.getClassName().endsWith(descriptor.getMethodName());
    }

    @Override
    public void before(Object o, Object[] objects) {
        Trace trace = traceContext.currentTraceObject();
        SpanEventRecorder spanEventRecorder = trace.traceBlockBegin();

        record(spanEventRecorder, o);
        myRecordBefore(o);
    }


    private void record(SpanEventRecorder spanEventRecorder, Object o){
        spanEventRecorder.recordServiceType(WebRTSTypeProvider.SERVICE_TYPE);

        Set<Record> records = new HashSet<>();
        records.add(RecordManager.get(descriptor));
        if(o != null)
            records.add(new ClassRecord(o.getClass().getName(), RecordManager.getClassFilePath(o.getClass())));
        Set<Record> inits = RecordManager.getObject(o);
        if (inits != null)
            records.addAll(inits);


        spanEventRecorder.recordApi(descriptor);
        spanEventRecorder.recordAttribute(WebRTSTypeProvider.CLASSES_ANNOTATION_KEY, JsonUtils.encodeRecord(records));

    }

    @Override
    public void after(Object o, Object[] objects, Object o1, Throwable throwable) {
        myRecordAfter();
        Trace trace = traceContext.currentTraceObject();
        trace.traceBlockEnd();
    }

    private void myRecordBefore(Object o){
        // ThreadContext.record(RecordManager.get(descriptor));
        if(ENABLE_CONSTRUCTOR){
            if(isConstructor){
                ThreadContextStack.initBefore(o);
            }

            if(ThreadContextStack.isRecord()){
                ThreadContextStack.record(RecordManager.get(descriptor));
            }
        }
    }

    private void myRecordAfter(){
        if(ENABLE_CONSTRUCTOR){
            if(isConstructor){
                ThreadContextStack.initAfter();
            }
        }
    }
}
