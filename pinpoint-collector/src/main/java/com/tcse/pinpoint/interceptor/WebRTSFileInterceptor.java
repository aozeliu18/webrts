package com.tcse.pinpoint.interceptor;

import com.navercorp.pinpoint.bootstrap.context.MethodDescriptor;
import com.navercorp.pinpoint.bootstrap.context.SpanEventRecorder;
import com.navercorp.pinpoint.bootstrap.context.Trace;
import com.navercorp.pinpoint.bootstrap.context.TraceContext;
import com.navercorp.pinpoint.bootstrap.interceptor.AroundInterceptor;
import com.tcse.common.entity.FileRecord;
import com.tcse.common.JsonUtils;
import com.tcse.common.entity.Record;
import com.tcse.pinpoint.WebRTSTypeProvider;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

public class WebRTSFileInterceptor implements AroundInterceptor {

    private static String CONTEXT_PATH = System.getProperty("webrts.agent.context", "/usr/local/tomcat/webapps/");

    private TraceContext traceContext;
    private MethodDescriptor methodDescriptor;

    public WebRTSFileInterceptor(TraceContext traceContext, MethodDescriptor methodDescriptor) {
        this.traceContext = traceContext;
        this.methodDescriptor = methodDescriptor;
    }

    @Override
    public void before(Object o, Object[] objects) {
    }

    @Override
    public void after(Object o, Object[] objects, Object o1, Throwable throwable) {
        if(!isWebAppClassLoader()){
            return;
        }

        Trace trace = traceContext.currentTraceObject();
        if(trace == null){
            return;
        }

        recordFile(trace, o);
    }

    private boolean isWebAppClassLoader(){
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if(contextClassLoader == null){
            return false;
        }

        return !contextClassLoader.getClass().getName().
                equals("com.navercorp.pinpoint.bootstrap.classloader.ParallelClassLoader");
    }

    private void recordFile(Trace trace, Object o){
        if(o instanceof File){
            File file = (File)o;
            if(file.isDirectory()){
                return;
            }
            try {
                String canonicalPath = file.getCanonicalPath();
                if(isWebAppFile(canonicalPath) /*&& canonicalPath.startsWith(CONTEXT_PATH)*/){
                    //String path = canonicalPath.substring(CONTEXT_PATH.length());
                    trace.traceBlockBegin();
                    try{
                        SpanEventRecorder spanEventRecorder = trace.currentSpanEventRecorder();
                        spanEventRecorder.recordServiceType(WebRTSTypeProvider.SERVICE_TYPE);
                        spanEventRecorder.recordApi(methodDescriptor);
                        HashSet<Record> set = new HashSet<>();
                        set.add(new FileRecord(canonicalPath));
                        spanEventRecorder.recordAttribute(WebRTSTypeProvider.FILES_ANNOTATION_KEY, JsonUtils.encodeRecord(set));
                    }finally {
                        trace.traceBlockEnd();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isWebAppFile(String path){
        return !path.contains("com/navercorp/pinpoint");
    }
}
