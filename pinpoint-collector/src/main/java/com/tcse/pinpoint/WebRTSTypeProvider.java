package com.tcse.pinpoint;

import com.navercorp.pinpoint.common.trace.*;

public class WebRTSTypeProvider implements TraceMetadataProvider {

    public static final ServiceType SERVICE_TYPE = ServiceTypeFactory.of(7500,"WEBRTS");
    public static final AnnotationKey CLASSES_ANNOTATION_KEY = AnnotationKeyFactory.of(8922, "classes", AnnotationKeyProperty.VIEW_IN_RECORD_SET);
    public static final AnnotationKey FILES_ANNOTATION_KEY = AnnotationKeyFactory.of(8923, "files", AnnotationKeyProperty.VIEW_IN_RECORD_SET);

    @Override
    public void setup(TraceMetadataSetupContext traceMetadataSetupContext) {
        traceMetadataSetupContext.addServiceType(SERVICE_TYPE);
        traceMetadataSetupContext.addAnnotationKey(CLASSES_ANNOTATION_KEY);
        traceMetadataSetupContext.addAnnotationKey(FILES_ANNOTATION_KEY);
    }
}
