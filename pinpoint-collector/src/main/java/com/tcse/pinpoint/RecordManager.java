package com.tcse.pinpoint;


import com.navercorp.pinpoint.bootstrap.context.MethodDescriptor;
import com.tcse.common.JsonUtils;
import com.tcse.common.entity.ClassRecord;
import com.tcse.common.entity.FileRecord;
import com.tcse.common.entity.HttpTrace;
import com.tcse.common.entity.Record;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author aozeliu
 */
public class RecordManager {

    private static final Map<Object, Set<Record>> objects = new WeakHashMap<>();

    public static void putObject(Object object, Set<Record> records){
        Set<Record> oldRecords = objects.get(object);
        if(oldRecords != null){
            oldRecords.addAll(records);
        }else {
            objects.put(object, records);
        }
    }

    public static Set<Record> getObject(Object object){
        return objects.get(object);
    }

    private static final Set<HttpTrace> traces = new CopyOnWriteArraySet<>();

    public static void putTrace(HttpTrace trace){
        traces.add(trace);
    }

    public static String export(){
        String data = JsonUtils.transform(traces);
        traces.clear();
        return data;
    }


    public static ClassRecord get(MethodDescriptor methodDescriptor){
        String className = methodDescriptor.getClassName();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return new ClassRecord(className, getClassFilePath(classLoader, className));
    }

    public static FileRecord get(File file){
        try {
            String path = file.getCanonicalPath();
            FileRecord record = new FileRecord(path);
            return record;
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return new FileRecord(file.getAbsolutePath());
    }


    public static String getClassFilePath(Class clazz){
        ClassLoader classLoader = clazz.getClassLoader();
        String name = clazz.getName();
        return getClassFilePath(classLoader, name);
    }

    public static String getClassFilePath(ClassLoader classLoader, String classname){
        if(classLoader != null){
            String classfilePath = classname.replace(".", "/").concat(".class");
            URL url = classLoader.getResource(classfilePath);
            if(url == null){
                return null;
            }
            return url.toExternalForm();
        }
        return null;
    }
}
