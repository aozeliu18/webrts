package com.tcse.pinpoint;

import com.tcse.common.entity.Record;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * @author aozeliu
 */
public class ThreadContextStack {

    static class Recorder{
        Object object;
        Set<Record> records = new HashSet<>();
        Recorder(Object o){
            this.object = o;
        }
    }

    private LinkedList<Recorder> recorders;
    // current即栈顶，为了提高效率，栈顶元素不放入recorders中
    private Recorder current;

    /**
     * 当需要建立ThreadContextStack时，就意味着有对象被构造了
     * 所以构造ThreadContextStack也需要传入被构造的对象
     * @param object
     */
    public ThreadContextStack(Object object) {
        recorders = new LinkedList<>();
        current = new Recorder(object);
    }

    /**
     * 构造函数嵌套
     * 当在某个对象的构造函数中调用其他构造函数时，会调用此方法
     * 此时会先向先前的栈顶@code{current}放入recorders中
     * 然后新建一个recorder记录对象的构造依赖信息，并把这个recorder作为新的栈顶
     * @param object 即嵌套构造的对象
     */
    public void push(Object object){
        recorders.push(current);
        current = new Recorder(object);
    }
    public Recorder pop(){
        Recorder recorder = current;
        if(recorders.isEmpty()){
            current = null;
        }else{
            current = recorders.pop();
            current.records.addAll(recorder.records);
        }
        return recorder;
    }
    public boolean isEmpty(){
        return current == null;
    }
    public void addRecord(Record record){
        current.records.add(record);
    }


    private static final ThreadLocal<ThreadContextStack> stacks = new ThreadLocal<>();

    public static boolean isRecord(){
        return stacks.get() != null;
    }

    public static void record(Record record){
        stacks.get().addRecord(record);
    }

    /**
     * 构造方法执行前后
     */
    public static void initBefore(Object object){
        ThreadContextStack stack = stacks.get();
        if(stack == null){
            stack = new ThreadContextStack(object);
            stacks.set(stack);
        }else{
            stack.push(object);
        }
    }

    public static void initAfter(){
        ThreadContextStack stack = stacks.get();
        Recorder recorder = stack.pop();
        RecordManager.putObject(recorder.object, recorder.records);
        if(stack.isEmpty()){
            stacks.remove();
        }
    }
}
