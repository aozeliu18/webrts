package com.tcse.pinpoint;


import com.tcse.common.*;
import com.tcse.common.entity.Record;

import java.util.HashSet;
import java.util.Set;


/**
 * @author aozeliu
 */
public class ThreadContext {

    private Set<Record> records;


    public ThreadContext() {
        records = new HashSet<>();
    }

    public void addRecord(Record record){
        records.add(record);
    }
    public void addRecords(Set<Record> rs){
        records.addAll(rs);
    }


    /* ---------------------------------- 静态方法 -------------------------------------- */

    private static final ThreadLocal<ThreadContext> contexts = new ThreadLocal<>();

    private static ThreadContext get(){
        ThreadContext context = contexts.get();
        if(context == null){
            context = new ThreadContext();
            contexts.set(context);
        }
        return context;
    }

    public static void record(Record record){
        if(record != null) {
            get().addRecord(record);
        }
    }

    public static void records(Set<Record> records){
        if(records != null) {
            get().addRecords(records);
        }
    }
}
