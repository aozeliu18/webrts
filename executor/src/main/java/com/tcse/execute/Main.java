package com.tcse.execute;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static List<String> read(String filepath){
        List<String> tests = new ArrayList<>();
        try (Scanner scanner = new Scanner(new FileInputStream(filepath))){
            while (scanner.hasNext()){
                String testId = scanner.nextLine();
                tests.add(testId);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return tests;
    }

    public static void main(String[] args) {
        JunitTestExecutor executor = new JunitTestExecutor();
        executor.execute(read(args[0]));
    }
}
