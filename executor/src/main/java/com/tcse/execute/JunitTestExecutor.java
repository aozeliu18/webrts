package com.tcse.execute;

import org.junit.platform.engine.DiscoverySelector;
import org.junit.platform.engine.TestSource;
import org.junit.platform.engine.support.descriptor.ClassSource;
import org.junit.platform.engine.support.descriptor.MethodSource;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherConfig;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;

import java.util.*;

import static org.junit.platform.engine.discovery.DiscoverySelectors.*;

/**
 * @author aozeliu
 */
public class JunitTestExecutor {

    private Launcher launcher;

    public JunitTestExecutor(){
        initLauncher();
    }

    private void initLauncher(){
        LauncherConfig config = LauncherConfig.builder()
                .build();
        launcher = LauncherFactory.create(config);
    }


    private LauncherDiscoveryRequest getRequest(List<String> tests){
        List<DiscoverySelector> selectors = new ArrayList<>();
        for(String tid : tests){
            selectors.add(selectUniqueId(tid));
        }
        return LauncherDiscoveryRequestBuilder.request()
                .selectors(selectors)
                .configurationParameter("junit.jupiter.extensions.autodetection.enabled", "true")
                .build();
    }


    public void execute(List<String> tests){
        LauncherDiscoveryRequest request = getRequest(tests);
        launcher.execute(request);
    }


}
