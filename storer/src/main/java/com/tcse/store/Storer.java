package com.tcse.store;

import com.tcse.common.HttpTrace;
import com.tcse.common.Record;
import com.tcse.common.TestCaseInfo;

import java.util.List;
import java.util.Set;

/**
 * @author aozeliu
 */
public interface Storer {
    /**
     * 获取测试的依赖信息
     * @param test 测试信息
     * @return
     */
    Set<Record> get(TestCaseInfo test);

    /**
     * 存储数据到文件中
     * @param traces
     * @param path
     */
    void storeTraces(List<HttpTrace> traces, String path);
}
