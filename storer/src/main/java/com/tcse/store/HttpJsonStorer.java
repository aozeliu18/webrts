package com.tcse.store;

import com.tcse.common.*;

import java.io.*;
import java.util.*;

/**
 * @author aozeliu
 *
 * 三个任务
 * 1. 存储数据
 * 2. 读取数据
 * 3. 查询测试依赖的HttpTrace下的FileRecord
 */
public class HttpJsonStorer implements Storer{
    private List<HttpTrace> traces;

    public HttpJsonStorer(String depsPath) {
        this.traces = readTraces(depsPath);
    }

    /*
     * 读取数据
     */
    public List<HttpTrace> readTraces(String path){
        File file = new File(path);

        List<HttpTrace> list = null;

        StringBuilder sb = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = reader.readLine()) != null){
                sb.append(line);
            }
            list = GsonUtils.transform(sb.toString());
        } catch (FileNotFoundException e) {
            System.err.println("File " + path + " Doesn't exist !!!" );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /*
     * 存储数据
     */
    public void storeTraces(List<HttpTrace> newtraces, String path){
        Set<HttpTrace> traces = mergeTraces(newtraces, this.traces);
        File file = new File(path);
        try(PrintWriter writer = new PrintWriter(file)) {
            String data = GsonUtils.transform(traces);
            writer.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 合并trace集合,去掉重复trace
     * @return
     */
    private Set<HttpTrace> mergeTraces(Collection<HttpTrace> a, Collection<HttpTrace> b){
        Set<HttpTrace> res = new HashSet<>();
        if(a != null)
            res.addAll(a);
        if(b != null)
            res.addAll(b);
        return res;
    }

    /*
     * 查询数据
     */
    public Set<Record> get(TestCaseInfo test){
        Set<Record> set = new HashSet<>();

        if(traces != null) {
            for (HttpTrace trace : traces){
                if(trace.getTest() != null && test.getName().contains(trace.getTest())) {
                    set.addAll(trace.getRecords());
                }
            }
        }
        return set;
    }
}
