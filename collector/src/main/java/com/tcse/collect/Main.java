package com.tcse.collect;

import com.tcse.common.GsonUtils;
import com.tcse.common.HttpTrace;
import com.tcse.common.RTSConfig;
import com.tcse.store.HttpJsonStorer;
import com.tcse.store.Storer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aozeliu
 */
public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("Start collect............");
        long start = System.currentTimeMillis();

        if(args.length == 1){
            RTSConfig.loadConfig(args[0]);
        }else{
            RTSConfig.loadConfig(System.getProperty("webrts.config"));
        }

        String projectDir = RTSConfig.getConfig("projectDir");
        Transformer analyzer = new Transformer(projectDir);

        List<HttpTrace> traces = new ArrayList<>();

        // 从每一个节点上拉取遗漏信息
        String[] addresses = RTSConfig.getConfig("addresses").split(",");
        for(String address : addresses){
            String data = DataFetcher.fetch(address);
            List<HttpTrace> nodeTraces = GsonUtils.transform(data);
            traces.addAll(nodeTraces);
        } 

        // 分析数据
        analyzer.analyze(traces);

        String depsPath = RTSConfig.getConfig("depsPath");
        Storer storer = new HttpJsonStorer(depsPath);
        storer.storeTraces(traces, depsPath);

        long end = System.currentTimeMillis();
        LOGGER.info("Total Time Cost : " + (end - start) + "ms");
        LOGGER.info("Finished collect............");
    }
}
