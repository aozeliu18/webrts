package com.tcse.collect;

import com.tcse.common.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author aozeliu
 * 通过分析字节码获取依赖的文件
 *
 * 对于文件记录，计算校验和
 * 对于方法记录，转换为文件记录
 *
 */
public class Transformer{

    private static final Logger LOGGER = LoggerFactory.getLogger(Transformer.class);

    private String projectDir;
    private Hasher hasher;



    public Transformer(String projectDir) {
        this.projectDir = projectDir;
        hasher = new HasherImpl();
    }

    public void analyze(List<HttpTrace> traces){
        for(HttpTrace trace : traces){
            Set<Record> fileRecords = analyze(trace.getRecords());
            trace.setRecords(fileRecords);
        }
    }

    public Set<Record> analyze(Collection<? extends Record> records) {
        Set<Record> ans = new HashSet<>();
        for(Record record : records){
            if(record instanceof MethodRecord){
                Set<FileRecord> fileRecords = handleMethodRecord((MethodRecord) record);
                ans.addAll(fileRecords);
            }else if(record instanceof FileRecord){
                FileRecord fileRecord = handleFileRecord((FileRecord) record);
                ans.add(fileRecord);
            }
        }
        return ans;
    }

    /**
     * 为record代表文件记录添加校验和
     */
    protected FileRecord handleFileRecord(FileRecord record){
        String path = PathUtils.getRelatedPath(record.getPath());
        FileRecord fileRecord = createFile(path);
        return fileRecord;
    }


    /**
     * 将方法记录转换为文件记录
     * 如果实例对象类型和方法所属类型不同，则进行类层次结构分析，找出介于两则之间的所有类
     */
    protected Set<FileRecord> handleMethodRecord(MethodRecord record){
        Set<FileRecord> fileRecords = new HashSet<>();

        String methodClass = record.getMethodClass();
        FileRecord methodClassRecord = createClassFile(methodClass);
        fileRecords.add(methodClassRecord);

        String instanceClassPath = record.getInstanceClass();
        if(instanceClassPath != null) {
            FileRecord instanceClassRecord = createClassFile(instanceClassPath);
            fileRecords.add(instanceClassRecord);
        }

        return fileRecords;
    }

    /**
     * 计算类文件的相对路径
     */
    protected FileRecord createClassFile(String classname){
        String path = PathUtils.getClassPath(classname);
        return createFile(path);
    }

    /**
     * 创建含有校验和的文件记录
     */
    protected FileRecord createFile(String path){
        File file = new File(projectDir, path);
        FileRecord record = new FileRecord(path, hasher.hash(file));
        return record;
    }


    /**
     * 查找类路径，用于javaassit分析类层次结构
     * 主要包含两块
     *  1. classes目录
     *  2. lib目录下的所有jar包
     * @param projectDir Web项目的根路径
    * @return 找到的类路径
     */
    private List<String> getClasspath(String projectDir){
        List<String> paths = new ArrayList<>();

        File libDir = new File(projectDir, "WEB-INF/lib");
        for (File f : libDir.listFiles()){
            if(f.getName().endsWith(".jar")){
                try {
                    paths.add(f.getCanonicalPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        File classDir = new File(projectDir, "WEB-INF/classes");
        try {
            paths.add(classDir.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paths;
    }
}
