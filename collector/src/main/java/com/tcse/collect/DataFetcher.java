package com.tcse.collect;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * @author aozeliu
 */
public class DataFetcher {

    public static final String URL_PATTERN = "http://%s:%d/%s/%s";
    public static final String BASE_URL = "sandbox/default/module/http";
    public static final String EXPORT_URL = "collector/export";

    private static String host;
    private static int port;

    public static void set(String host, int port){
        DataFetcher.host = host;
        DataFetcher.port = port;
    }

    public static String getURL(String uri){
        return String.format(URL_PATTERN, host, port, BASE_URL, uri);
    }

    public static String getURL(String host, int port, String uri) {
        return String.format(URL_PATTERN, host, port, BASE_URL, uri);
    }

    public static String flush(){
        return request(getURL("sandbox-module-mgr/flush?force=true"));
    }

    public static String start(String classPattern){
        return request(getURL("collector/trace?class=" + classPattern));
    }


    public static String unload(String modulePattern){
        return request(getURL("sandbox-module-mgr/unload?action=unload&ids=" + modulePattern));
    }

    public static String fetch(){
        return request(getURL(EXPORT_URL));
    }

    public static String fetch(String address) {
        String[] s = address.split(":");
        String host = s[0];
        int port = Integer.valueOf(s[1]);
        return fetch(host, port);
    }

    public static String fetch(String host, int port) {
        return request(getURL(host, port, EXPORT_URL));
    }

    public static String request(String url){
        String result = null;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}

